#include "ExtLibrary.h"
#include <ARKE/XML.h>
#include "ExtManager.h"
#include <dlfcn.h>

namespace C3AI {
    ExtLibrary::ExtLibrary() : Loadable(), linked(false) {
        
    }
    
    ExtLibrary::ExtLibrary(const std::string& p) : ExtLibrary() {
        this->load(p);
    }
    
    ExtLibrary::~ExtLibrary() {
        
    }
    
    bool ExtLibrary::couldLoad(const std::string& p) const {
        return p.size() >= 4 && p.find(".xml") != std::string::npos;
    }

    bool ExtLibrary::load(const std::string& p = "") {
        if(p.size() > 0) Loadable::path = p;
        if(!Loadable::load(Loadable::path)) return false;
        
        XMLDoc doc;
        if(!doc.load(Loadable::path, false)) {
            Console::Warn("Extension Library XML file at ",Loadable::path," could not be parsed... Aborting.");
            return false;
        }
        
        xmlPath = Loadable::path;

        XMLNode root = doc.child("extension");
        if(!root) return false;
        
        this->id = root.get<std::string>("id");
        this->type = root.get<std::string>("type");
        this->name = root["name"].get<std::string>();
        
        //std::cout << "ID: " << this->id << "\n";
        
        for(XMLNode lib = root["library"]; bool(lib); lib = lib.sibling()) {
            auto pt = lib.get<std::string>("obj");
            if(pt.empty()) continue;
            
            auto pfx = lib.get<std::string>("pfx");
            //std::cout << "got prefix " << pfx << "\n";
            if(pfx.empty()) {
                //TODO: Error reporting
                std::cerr << "Library " << pt << " failed to provide entry point prefix. Skipping...\n";
                continue;
            }
            
            bool abs = lib.get<bool>("abs", false);
            libs.push_back(LibData{pt, pfx, nullptr, abs});
            //std::cout << "\t" << libs.back().getFullPath() << "\t" << abs << "\n";
        }
        
        for(XMLNode lib = root["dep"]; lib; lib = lib.sibling()) {
            deps.push_back(lib.get<std::string>("id"));
        }
        
        for(XMLNode lib = root["conflict"]; lib; lib = lib.sibling()) {
            conflicts.push_back(lib.get<std::string>("id"));
        }
        
        return true;
    }
    
    bool ExtLibrary::isLoaded() const {
        return !this->id.empty();
    }
    
    std::string ExtLibrary::getName() const {
        return this->id;
    }
    
    void ExtLibrary::unload() {
        
    }
    
    ExtLibrary* ExtLibrary::Factory(const std::string p) {
        ExtLibrary* ret = new ExtLibrary();
        if(p.size() == 0) return ret;
        
        if(ret->load(p)) return ret;
        
        delete ret;
        return nullptr;
    }
    
    bool ExtLibrary::link() {
        if(linked) return true;
        
        bool success = true;
        
        for(auto& lib : libs) {
            lib.handle = dlopen(lib.getFullPath().c_str(), RTLD_NOW | RTLD_GLOBAL);
            
            success &= lib.handle != nullptr;
            
            if(!success) {
                std::cerr << "Failed to load library at " << lib.getFullPath() << " due to " << dlerror() << "\n";
                break;
            }
        }
        
        if(success) {
            for(auto& lib : libs) {
                void (*fn)();
                const std::string entryPoint = lib.prefix + std::string("_OnLink");
                
                fn = reinterpret_cast<decltype(fn)>(dlsym(lib.handle, entryPoint.c_str()));
                
                if(fn != nullptr) (*fn)();
            }
        }
        
        return success;
    }
    
    bool ExtLibrary::unlink() {
        if(!linked) return true;
        
        for(auto& lib : libs) {
            void (*fn)();
            const std::string exitPoint = lib.prefix + std::string("_OnUnlink");
            
            fn = reinterpret_cast<decltype(fn)>(dlsym(lib.handle, exitPoint.c_str()));
            if(fn != nullptr) (*fn)();
            
            dlclose(lib.handle);
            lib.handle = nullptr;
        }
        linked = false;
        return true;
    }
    
    std::string ExtLibrary::LibData::getFullPath() const {
        if(absolutePath) return path;
        else return Extensions->extensionPath() + path;
    }
}
