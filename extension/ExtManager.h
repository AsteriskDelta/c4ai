#ifndef C3AI_EXT_MANAGER_H
#define C3AI_EXT_MANAGER_H
#include "C3Include.h"
#include "ExtLibrary.h"
#include <ARKE/LoadableManager.h>

namespace C3AI {
    class ExtManager : public LoadableManager<ExtLibrary> {
    public:
        typedef LoadableManager<ExtLibrary> Super;
        
        ExtManager();
        virtual ~ExtManager();
        
        bool canLink(ExtLibrary *lib);
        bool requestLink(const std::string& id);
        bool requestLink(ExtLibrary *lib);
        
        void wasLinked(ExtLibrary *lib);
        void wasUnlinked(ExtLibrary *lib);
        
        virtual void wasLoaded(ExtLibrary* ptr) override;
        
        inline const std::string& extensionPath() const {
            return extPath;
        }
        
        std::unordered_set<ExtLibrary*> extensions;
        std::unordered_set<ExtLibrary*> linked;
        std::unordered_map<std::string, ExtLibrary*> byID;
        
        std::unordered_map<std::string, std::unordered_set<ExtLibrary*>> byType;
    protected:
        std::unordered_map<std::string, ExtLibrary*> blacklisted;
        
        std::string extPath;
        
    };
    
    extern ExtManager *Extensions;
};

#endif
