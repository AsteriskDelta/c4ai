#ifndef C3AI_EXT_LIB_H
#define C3AI_EXT_LIB_H
#include "C3Include.h"
#include <ARKE/Loadable.h>

namespace C3AI {
    class ExtManager;
    
    class ExtLibrary :  public Loadable {
    friend class ExtManager;
    public:
        typedef ExtLibrary Self;
        
        ExtLibrary();
        ExtLibrary(const std::string& p);
        virtual ~ExtLibrary();
        
        virtual bool couldLoad(const std::string& p) const override;
        virtual bool load(const std::string& p) override;
        virtual void unload() override;
        
        virtual std::string getName() const override;
        
        inline virtual void* rawInstancePtr() override { return reinterpret_cast<void*>(this); };
        
        virtual bool isLoaded() const;
        
        static Self* Factory(const std::string p = "");
        
        bool link();
        bool unlink();
        inline bool isLinked() const {
            return this->linked;
        }
        
        std::string id, name, type, xmlPath;
        struct LibData {
            std::string path, prefix;
            void *handle;
            bool absolutePath;
            
            std::string getFullPath() const;
        };
        std::list<LibData> libs;
        
        std::list<std::string> deps, conflicts;//, provides;
    
    protected:
        bool linked;
    };
};

#endif

