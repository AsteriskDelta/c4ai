#include "ExtManager.h"
#include "ExtLibrary.h"

//TODO: Blacklisting backend

namespace C3AI {
    ExtManager *Extensions = new ExtManager();
    
    ExtManager::ExtManager() : ExtManager::Super() {
        Extensions = this;
        extPath = "build/Desktop/";//"extensions/";
    }
    
    ExtManager::~ExtManager() {
        if(Extensions == this) Extensions = nullptr;
    }
    
    bool ExtManager::canLink(ExtLibrary *lib) {
        if(lib == nullptr) return false;
        
        auto it = blacklisted.find(lib->id);
        if(it != blacklisted.end()) return false;
        
        return true;
    }
    
    bool ExtManager::requestLink(const std::string& id) {
        auto it = byID.find(id);
        if(it == byID.end()) return false;
        else return this->requestLink(it->second);
    }
    bool ExtManager::requestLink(ExtLibrary *lib) {
        return lib->link();
    }
    
    void ExtManager::wasLinked(ExtLibrary *lib) {
        linked.insert(lib);
    }
    void ExtManager::wasUnlinked(ExtLibrary *lib) {
        linked.erase(lib);
    }
    
    void ExtManager::wasLoaded(ExtLibrary* lib) {
        if(lib == nullptr) return;
        Super::wasLoaded(lib);
        
        extensions.insert(lib);
        byID[lib->id] = lib;
        
        if(!lib->type.empty()) byType[lib->type].insert(lib);
    }
}
