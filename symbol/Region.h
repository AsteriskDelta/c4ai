#ifndef C3AI_REGION_H
#define C3AI_REGION_H
#include "C3Include.h"
#include "Symlink.h"
#include <vector>

namespace C3AI {
    class Region {
    public:
        Region();
        ~Region();
        
        Num complexity() const;
        Num overlap(RegionPtr &o);
        
        Num evaluate(const Symbol *const coreSym) const;
    protected:
        std::vector<Symlink> links;
        Num prob;
    };
}

#endif
