#include "Symlink.h"
#include <NVX/NGaussian.impl.h>

namespace nvx {
    template class Gaussian<::C3AI::Num>;
};

namespace C3AI {
    Symlink::Symlink() : Distrib(), symbol(nullptr) {
        
    }
    
    Symlink::Symlink(SymbolPtr t) : Distrib(), symbol(t) {
        
    }
    
    void Symlink::update() {
        Num range = this->invpdf(Epsilon);
        cache.min = this->mean() - range;
        cache.max = this->mean() + range;
    }
    
    //Should we even bother checking?
    bool Symlink::intersects(const Symlink& o) {
        return  this->symbol == o.symbol &&
                !(*this > o || o < *this);
    }
    
    //Integral of probability shared with other distribution
    Num Symlink::intersection(const Symlink& o) const {
        return Distrib::Vertex(this, &o);
    }
    Num Symlink::intersection2(const Symlink& o) const {
        return sqrt(this->intersection(o));
    }
    
    //Probability (0-1) of expected value being contained by the other distribution
    Num Symlink::contains(const Symlink& o) const {
        return this->pdf(o.mean());
    }
    Num Symlink::contained(const Symlink& o) const {
        return o.pdf(this->mean());
    }
    
    Num Symlink::affinity(const Symlink& o) const {
        return sqrt(this->contains(o) * this->contained(o));
    }
    
    //Weighted score of peak distribution intersection
    Num Symlink::score(const Symlink &o) const {
        const Num prb = this->intersect(o);//In CDF of this
        
        const Self *ag, *bg;
        if(this->mean() < o.mean()) {
            ag = this; bg = &o;
        } else {
            ag = &o; bg = this;
        }
        
        //Find values that produce the probablities (l/r of mean not defined)
        Num av = ag->invpdf(prb), bv = bg->invpdf(prb);
        
        //Ensure a's prob is greater, b's is less
        av = ag->mean() + abs(ag->mean() - av);
        bv = bg->mean() - abs(bg->mean() - bv);
        
        const Num   aProb =  Num(1) - ag->cdf(av), 
                    bProb = bg->cdf(bv);
                    
        return aProb * ag->weight + bProb * bg->weight;
    }
}
