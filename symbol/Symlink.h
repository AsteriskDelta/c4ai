#ifndef C3AI_SYMLINK_H
#define C3AI_SYMLINK_H
#include "C3Include.h"
#include <NVX/NGaussian.h>

namespace C3AI {
    class Symbol;
    
    class Symlink : public Gaussian<Num> {
    public:
        typedef Gaussian<Num> Distrib;
        typedef Symlink Self;
        
        Symlink();
        Symlink(SymbolPtr t);
        
        //Build the cache;
        void update();
        
        //Should we even bother checking?
        bool intersects(const Symlink& o);
        
        //Integral of probability shared with other distribution
        Num intersection(const Symlink& o) const;
        Num intersection2(const Symlink& o) const;
        
        //Probability (0-1) of expected value being contained by the other distribution
        Num contains(const Symlink& o) const;
        Num contained(const Symlink& o) const;
        Num affinity(const Symlink& o) const;
        
        //Weighted score of peak distribution intersection
        Num score(const Symlink &o) const;

        SymbolPtr symbol;
        Num weight;
        
        inline operator bool() const {
            return symbol != nullptr;
        }
        
        inline operator<(const Symlink& o) const {
            return this->cache.max < o.cache.min;
        }
        inline operator>(const Symlink& o) const {
            return this->cache.min > o.cache.max;
        }
    protected:
        constexpr const static Num Epsilon = Num(0.001);
        
        struct Cache {
            Num min, max;
        } cache;
    };
}

#endif
