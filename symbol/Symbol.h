#ifndef C3AI_SYMBOL_H
#define C3AI_SYMBOL_H
#include "C3Include.h"
#include <vector>

namespace C3AI {
    class Intrinsic;
    extern ptr_t IntrinsicSize;
    
    class Symbol {
    public:
        Symbol();
        ~Symbol();
        
        inline Intrinsic* intrinsic() {
            if(!flags.intrinsic) return nullptr;
            else {
                 ptr_t ptr = reinterpret_cast<ptr_t>(this);
                 ptr = ptr - IntrinsicSize;
                 return reinterpret_cast<Intrinsic*>(ptr);
            }
        }
        
        struct Flags {
            bool intrinsic : 1;
        } flags;
        
        
        struct RegionData {
            RegionPtr ptr;
            Num weight, prob;
            inline operator RegionPtr&() { return ptr; }
            inline operator const RegionPtr&() const { return ptr; }
        };
        std::vector<RegionData> regions;
    protected:
        
    };
}

#endif
