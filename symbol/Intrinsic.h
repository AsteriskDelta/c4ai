#ifndef C3AI_INTRIN_H
#define C3AI_INTRIN_H
#include "C3Include.h"
#include "Symbol.h"

namespace C3AI {
    class ExtLibrary;
    
    class Intrinsic : public Symbol {
    public:
        Intrinsic();
        
        inline bool isOperation() {
            return data.handler != nullptr;
        }
        inline const std::string& name() const {
            return data.name;
        }
    protected:
        struct Data {
            void (*handler)(Symbol*) = nullptr;
            ExtLibrary *owner;
            std::string name;
        } data;
    };
    
    namespace Intrin {
        
    };
};

#endif
