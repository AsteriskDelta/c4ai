#include "Intrinsic.h"

namespace C3AI {
    ptr_t IntrinsicSize = static_cast<ptr_t>(sizeof(Intrinsic));
}
