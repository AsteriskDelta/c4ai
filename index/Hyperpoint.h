#ifndef C3AI_HYPERPT_H
#define C3AI_HYPERPT_H
#include <ARKE/SmallMap.h>

namespace C3AI {
    
    class Hyperpoint : public SmallMap<Symbol*, Num> {
    public:
        typedef SmallMap<Symbol*, Num> Parent;
        using Parent::Parent;
    protected:
        
    }
}

#endif
