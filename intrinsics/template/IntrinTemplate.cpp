#include "IntrinTemplate.h"

namespace C3AI {
    namespace Intrin {
        
    }
}

extern "C" void IntrinTemplate_OnLink() {
    std::cout << "It works!\n";
    std::cout << "Extensions ptr at " << C3AI::Extensions << "\n";
    
    for(auto& lib : C3AI::Extensions->extensions) {
        std::cout << "\t" << lib->id << "\t" << lib->name << "\n";
    }
}

extern "C" void IntrinTemplate_OnUnlink() {
    std::cout << "Template dyn lib says goodbye!\n";
}
