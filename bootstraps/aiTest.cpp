#include "C3AI.h"
#include "extension/ExtManager.h"

using namespace C3AI;

int main(int argc, char** argv) {
    std::cout << "Dyn lib load:\n";
    Extensions->loadFromDir("extensions");
    Extensions->byID["IntrinTemplate"]->link();
    return 0;
}
