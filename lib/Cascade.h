#ifndef CASCADE_VECTOR_H
#define CASCADE_VECTOR_H
#include "GenIterator.h"

#include "C3AI.h"

template<typename V>
class Cascade {
public:
    typedef Cascade<V> Self;
    typedef V ValuePtr;
    
    Cascade();
    ~Cascade();
    
    ValuePtr cascade();
    inline bool cascaded() const noexcept {
        return cscParent != nullptr;
    }
    
    void clear();
protected:
    ValuePtr cscParent, cscChild;
    Num cascadeFactor;
    Num cascadeOffset;
    unsigned short cscDepth, cscMaxDepth;
    
    inline bool isRoot() const noexcept {
        return cscDepth == 0;
    }
public:
    
};

#endif

