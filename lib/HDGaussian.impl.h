#include "HDGaussian.h"
#define HDG_TPL template<typename PT>
#define HDG_T HDGaussian<PT>

HDG_TPL
HDG_T::HDGaussian() : invalid(true) {
    
}
HDG_TPL
HDG_T::~HDGaussian() {
    
}

HDG_TPL
void HDG_T::set(const typename HDG_T::Point& mm, const typename HDG_T::Point& vv) {
    //std::cout << "HDI GAUSSIAN SET mean=" << mm << ", var=" << vv << "\n";
    for(const auto& pair : mm) {
        (*this)[pair.key].set(mm[pair.key], vv[pair.key]);
    }
    cache.mean = mm;
    cache.variance = vv;
    if(cache.variance.magnitude2() < 0.0001) {
        cache.meanProb = cache.mean.ones();
        invalid = true;
    } else {
        invalid = false;
        cache.meanProb = this->pdf(cache.mean);
    }
}

HDG_TPL
typename HDG_T::Point HDG_T::pdf(const typename HDG_T::Point& state, bool rel) const {
    if(this->invalid) return state.zeros();
    
    Point ret;
    //std::cout << "\t\t\tHDIGaussian working from mean=" << cache.mean << ", var=" << cache.variance << ", meanProb=" << cache.meanProb << "\n";
    for(const typename Map::Pair& pair : *this) {
        //std::cout << "\t\tgaussian query = " << pair.value.pdf(state[pair.key]) << "\n";
        ret[pair.key] += pair.value.pdf(state[pair.key]);
        ret[pair.key] = std::min(typename Point::Value(1.0), ret[pair.key]);
    }
    if(rel) ret /= cache.meanProb;
    return ret;
}

HDG_TPL
typename HDG_T::Point HDG_T::cdf(const typename HDG_T::Point& state) const {
    if(this->invalid) return state.zeros();
    Point ret;
    for(const typename Map::Pair& pair : *this) {
        ret[pair.key] += pair.value.cdf(state[pair.key]);
    }
    return ret;
}

HDG_TPL
Num HDG_T::pdfProduct(const typename HDG_T::Point& state, bool rel) const {
    if(this->invalid) return 0.0;
    Num ret = 1.0;
    for(const typename Map::Pair& pair : *this) {
        //if(pair.value.variance() == 0) continue;
        //std::cout << "\t\tgaussian product query = " << pair.value.pdf(state[pair.key]) << "\n";
        ret *= pair.value.pdf(state[pair.key]) / (rel? pair.value.pdf(pair.value.mean()) : 1.0);
    }
    return ret;
}


HDG_TPL
typename HDG_T::Point HDG_T::invpdf(const typename HDG_T::Point& state) const {
    Point ret;
    for(const typename Map::Pair& pair : *this) {
        ret[pair.key] = pair.value.invpdf(state[pair.key]);
    }
    return ret;
}
HDG_TPL
typename HDG_T::Point HDG_T::invpdf(const Num& chance) const {
    Point ret;
    for(const typename Map::Pair& pair : *this) {
        ret[pair.key] = pair.value.invpdf(chance);
    }
    return ret;
}
/*
HDG_TPL
typename HDG_T::Point HDG_T::stddev() const {
    Point ret;
    for(const typename Map::Pair& pair : *this) {
        ret[pair.key] = pair.value.stddev();
    }
    return ret;
}
*/
