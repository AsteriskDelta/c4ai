#ifndef GENITERATOR_H
#define GENITERATOR_H
#include <vector>

template<typename VALUE>
class GenIterator : public std::iterator<std::bidirectional_iterator_tag, VALUE> {
public:
    typedef GenIterator<VALUE> Self;
    typedef VALUE Value;

    /*inline GenIterator() {
            
    }*/

    /*inline GenIterator(const GenIterator& orig) {
        _unused(orig);
    }*/
    inline virtual ~GenIterator() {

    }

    inline explicit operator bool() const noexcept {
        return this->valid();
    }

    inline Value* operator->() const {
        return this->ptr();
    }

    inline Value& operator*() const {
        return *(this->operator->());
    }

    inline Self& operator++() {
        this->advance();
        return *this;
    }

    inline Self& operator--() {
        this->retreat();
        return *this;
    }

    inline bool operator==(const Self& o) const noexcept {
        return this->equal(o.iterPtr());
    }

    inline bool operator!=(const Self& o) const noexcept {
        return !(this->equal(o.iterPtr()));// && this->valid();
    }

    //GenIterator next() const;
    //GenIterator prev() const;

    inline bool valid() const noexcept {
        return this->ptr() != nullptr;
    }
    inline constexpr bool hasKey() const {
        return true;
    }

protected:

    inline virtual void advance() {

    }

    inline virtual void retreat() {

    }

    inline virtual bool equal(const void *const& oPtr) const noexcept {
        const Self& o = *reinterpret_cast<const Self*>(oPtr);
        return this->ptr() == o.ptr();
    }

    
    inline virtual const void* iterPtr() const noexcept {
        return reinterpret_cast<const void*>(this);
    }
    
    inline virtual Value* ptr() const {
        return nullptr;
    }
    
private:

};

template<typename PARENT, typename VALUE, typename IDX=unsigned int>
class IndexIterator : public GenIterator<VALUE> {
public:
    IndexIterator(const PARENT parPtr, const IDX& i) : GenIterator<VALUE>(), parent(parPtr), idx(i) {};
    
    PARENT parent;
    IDX idx;
    
    inline operator const IDX& () const {
        return idx;
    }
    
    inline virtual void advance() override {
        idx++;
    }
    
    inline virtual void retreat() override {
        idx--;
    }
    
    //Delay insantiation until parent type is fully defined
    template<class=void>
    inline bool valid() const noexcept {
        return parent != nullptr && parent->size() > idx;
    }
    
    template<class=void>
    inline VALUE* ptr() const {
        return &parent->operator[](idx);
    }
    
    inline constexpr bool hasKey() const {
        return false;
    }
};

template<typename PARENT_PTR_T, typename VALUE, typename IDX=unsigned int>
class IndexIteratorHolder {
public:
    typedef IndexIterator<PARENT_PTR_T, VALUE, IDX> iterator;
    typedef IndexIteratorHolder<PARENT_PTR_T, VALUE, IDX> Self;
    
    template<class=void>
    inline iterator begin() const {
        return iterator(reinterpret_cast<PARENT_PTR_T>(
            const_cast<Self*>(this)
        ), 0);
    }
    template<class=void>
    inline iterator end() const {
        return iterator(reinterpret_cast<PARENT_PTR_T>(
            const_cast<Self*>(this)
        ), reinterpret_cast<PARENT_PTR_T>(
            const_cast<Self*>(this)
        )->size());
    }
    
};

#endif /* GENITERATOR_H */

