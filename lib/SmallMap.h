#ifndef SMALLMAP_H
#define SMALLMAP_H
#include "C4AI.h"
#include "GenIterator.h"
#include "VectorArith.h"
#include <Spinster/Common.h>

struct SmallMap_Root{};

template<typename KEY, typename VALUE>
class SmallMap : public VectorArith<SmallMap<KEY,VALUE>*>, public Spinster::Common, public SmallMap_Root {
public:
    typedef unsigned short Idt;
    typedef KEY Key;
    typedef VALUE Value;
    typedef VALUE value;
    typedef SmallMap<KEY,VALUE> Self;
    typedef VectorArith<SmallMap<KEY,VALUE>*> Arith;
    
    SmallMap();
    SmallMap(const SmallMap& orig);
    ~SmallMap();
    
    bool has(const Key& key) const noexcept;
    const Value& at(const Key& key) const;
    Value& at(const Key& key, const bool& create = false);
    
    inline const Value& operator[](const Key& key) const {
        return this->at(key);
    }
    inline Value& operator[](const Key& key) {
        return this->at(key, true);
    }
    
    struct Pair {
        Key key = Key();
        Value value = Value();
        bool valid() const;
        void invalidate();
        
        inline Pair& operator=(const Value& v) {
            value = v;
            return *this;
        };
        template<typename T>
        inline typename std::enable_if<std::is_convertible<T, Value>::value, Pair&> operator=(const Value& v) {
            value = v;
            return *this;
        };
        inline operator Value&() {
            return value;
        };
        inline operator const Value&() const {
            return value;
        };
    };
    struct PairRef {
        Key key = Key();
        Value &value;
        inline bool valid() const { return true; };
        inline void invalidate() {};
        
        inline PairRef& operator=(const Value& v) {
            value = v;
            return *this;
        };
        template<typename T>
        inline typename std::enable_if<std::is_convertible<T, Value>::value, PairRef&> operator=(const Value& v) {
            value = v;
            return *this;
        };
        inline operator Value&() {
            return value;
        };
        inline operator const Value&() const {
            return value;
        };
    };
    typedef const Pair ConstPair;
    
    const Pair* get(const Key& key) const;
    Pair* get(const Key& key, const bool& create = false);
    
    struct iterator : public GenIterator<Pair> {
        std::vector<Pair> *vec;
        Idt idx;
        
        inline iterator(const std::vector<Pair> *const& v, const Idt& i) :
        vec(const_cast<std::vector<Pair> *const>(v)), idx(i) {};
        operator Key&() const;
        
        void advance();
        void retreat();
        //bool equal(const iterator& o) const noexcept override;
        Pair* ptr() const;
        inline virtual bool equal(const void *const& oPtr) const noexcept override {
            const iterator& o = *reinterpret_cast<const iterator*>(oPtr);
            return idx == o.idx;
        }
        inline virtual const void* iterPtr() const noexcept override {
            return reinterpret_cast<const void*>(this);
        }
    };
    
    void clear();
    bool erase(const Key& key);
    Pair* insert(const Key& key, const Value& val, const bool& force = false);
    
    inline size_t size() const noexcept {
        return values.size() - unused.size();
    }
    inline bool empty() const noexcept {
        return this->size() == 0;
    }
    inline size_t allocated() const noexcept {
        return values.size();
    }
    
    iterator begin() const;
    iterator end() const;
    iterator iter(const Key& key) const;
    
    inline Idt getID(const Pair *const& pair) const;
    
    std::string str() const;
private:
    
    std::vector<Pair> values;
    std::vector<Idt> unused;
    static thread_local Value nilValue;
public:
    VA_EXT_EQ(+);
    VA_EXT_EQ(-);
    VA_EXT_EQ(*);
    VA_EXT_EQ(/);
    VA_EXT_EQ(&);
    VA_EXT_EQ(|);
    
#ifndef SMALLMAP_NO_ARITH
    inline Num product() const noexcept {
        if(this->size() == 0) return Num(0);
        
        Num ret = 1.0;
        for (auto it = this->begin(); it != this->end(); ++it) {
            ret *= static_cast<Num>((it->value));
        }
        return ret;
    }
    inline Num prismaticMagnitude() const noexcept {
        if(this->size() == 0) return Num(0);
        
        Num ret = 1.0;
        //Num ret = 0.0;//Simple n-dimensional distance vs. hypercube
        for (auto it = this->begin(); it != this->end(); ++it) {
            //ret += static_cast<Num>((it->value));
            ret *= static_cast<Num>((it->value));
        }
        //return Num(sqrt(ret / Num(this->size())));
        return ret;
    }
    inline auto prismatic() const noexcept {
        return this->prismaticMagnitude();
    }
    inline auto magnitude2() const noexcept {
        Num ret = 0.0;
        for (auto it = this->begin(); it != this->end(); ++it) {
            ret += static_cast<Num>((it->value) * (it->value));
        }
        return ret;
    }
    inline auto magnitude() const {
        return sqrt(this->magnitude2());
    }
    template<typename T>
    inline auto distanceTo2(const T& o) const {
        return (o - *this).magnitude2();
    }
    template<typename T>
    inline auto distanceTo(const T& o) const {
        return (o - *this).magnitude();
    }
    
    template<typename T>
    inline auto dot(const T& o) const noexcept {
        Num ret = 0.0;
        for (auto it = this->begin(); it != this->end(); ++it) {
            ret += static_cast<Num>((it->value) * o[it->key]);
        }
        return ret;
    }
    
    inline auto mask() const noexcept {
        Self ret;
        for (auto it = this->begin(); it != this->end(); ++it) {
            ret[it->key] = 1.0;
        }
        return ret;
    }
    inline auto ones() const noexcept {
        return this->mask();
    }
    inline auto without(const Self& patts) const noexcept {
        Self ret = *this;
        for(const Pair& pair : patts) ret.erase(pair.key);
        return ret;
    }
    inline auto zeros() const noexcept {
        Self ret;
        for (auto it = this->begin(); it != this->end(); ++it) {
            ret[it->key] = 0.0;
        }
        return ret;
    }
#endif
};

#endif /* SMALLMAP_H */

