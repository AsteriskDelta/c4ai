#ifndef MOD_HISTORY_H
#ifdef MOD_HISTORY_H_2PASS
#define MOD_HISTORY_H
#define _virt virtual
#define History VirtHistory
#else
#define _virt 
#endif
#include "C3AI.h"
#include <list>
#include "CyclicVector.h"
#include "Cascade.h"
#include "HistoryCache.h"

double CurrentTime();
void OverrideTime();
void StepTime(double dT);

    template<typename VALUE> class History;
#ifndef MOD_HISTORY_H_2PASS
    template<typename HValue> struct HistorySample {
        HValue value;
        Num weight;
        Time time;
        inline HistorySample(const HValue& val, const Num& w, const Time& t) : value(val), weight(w), time(t) {
        };
        inline ~HistorySample() {
            
        };
    };
    inline constexpr const Num SignificanceThreshold = 0.95;
    inline constexpr const Num KurtosisEpsilon = -0.333;
    extern Num SignificanceDeviation;
#endif
    
    
    template<typename VALUE>
    class History : public CyclicVector<HistorySample<VALUE>>, public Cascade<History<VALUE>*> {
    public:
        typedef VALUE Value;
        typedef History<VALUE> Self;
        typedef CyclicVector<HistorySample<VALUE>> Cyclic;
        typedef Cascade<History<VALUE>*> Cascade_T;
        typedef HistoryCache<VALUE> Cache;
        
        History();
        ~History();
        
        using Sample = HistorySample<VALUE>;
        
        struct {
            Num weight;
            unsigned int samples;
        } constraints;
        
        //Higher-level multisampling evaluation, blending based on the reciprocal of containing ranges
        Value evaluate(const Time& t);
        
        //Lower-level access- t must be contained, interpolates between known values
        Value sample(const Time& t);
        
        void setIdealWeight(const Num& weight);
        void setMaxSamples(const unsigned int& cnt);
        void setCascadeSamples(const unsigned int& cnt, const unsigned int& fracPercent = 0);
        void setCascadeFactor(Num factor);//Multiplier of weight for each subsequent cascade
        
        bool contains(const Time& t) const;
        
        //Compute variance, sigma, and range (if needed)
        bool frame();//Returns true if changed
        void clear();
        
        inline const Num& weight() const {
            return cascadeContrib.weight;
        }
        
        inline void pop();

        void push(const Value& val, const Num& weight = 1.0, const Time& t = 0.0);
        
        //Encorporate into own cache
        //void proxy(const Cache& oc);
        
        inline const Value& derivative() const {
            return cascadeContrib.derivative;
        }
        inline const Value& integral() const {
            return cascadeContrib.integral;
        }
        inline const Value& mean() const {
            return cascadeContrib.mean;
        }
        inline const Value& variance() const {
            return cascadeContrib.variance;
        }
        inline const Value& range() const {
            return cascadeContrib.range;
        }
        inline Value radius() const {
            return cascadeContrib.range/Num(2);
        }
        inline const Value& min() const {
            return cascadeContrib.min;
        }
        inline const Value& max() const {
            return cascadeContrib.max;
        }
        inline const Time& minTime() const {
            return cascadeContrib.minTime;
        }
        inline const Time& maxTime() const {
            return cascadeContrib.maxTime;
        }
        
        inline const Value& epsilon() const {
            return cascadeContrib.epsilon;
        }
        
        //Normalized (via popiviciu's inequality) reciprocal of variance
        inline const Value& sigma() const {
            return cascadeContrib.sigma;
        }
        
        inline const Value& skew() const {
            return cascadeContrib.skew;
        }
        inline const Value& kurtosis() const {
            return cascadeContrib.kurtosis;
        }
        inline const Value& stddev() const {
            return cascadeContrib.stddev;
        };
        inline Num ses() const {
            return cascadeContrib.ses;
        }
        inline Num sek() const {
            return cascadeContrib.sek;
        };
        
        //Distance of sigma from absolute noise (mag=0/axis) to absolute uniformity(mag=1/axis)
        inline const Num& consistence() const {
            return cascadeContrib.consistence;
        }
        
        //Samples per unit weight
        inline Num sampleRate() const {
             return this->size() / this->weight();
        }
        
    protected:
        void computeRange();
        void computeVariance();
        
        void setCascadeContrib(const Cache& oc);
        Cache& getCascadeContrib(Num factor);
        /*struct Cache {
            Num weight = 0.0, consistence = 0.0;
            Value mean = Value();
            Value range = Value(), min = Value(), max = Value();
            Value derivative = Value(), integral = Value();
            Value variance = Value(), maxVariance = Value();
            Value sigma = Value();
            Time time = 0.0;
            Time minTime =  std::numeric_limits<Time>::max(), maxTime = -std::numeric_limits<Time>::max();
            
            struct {
                bool range : 1;
                bool variance : 1;
                unsigned char padd : 6;
            } dirty;
        };*/
        Cache cache, cycleCache;
        Cache cascadeContrib;//The last contribution of the cascade's cache (to be subtracted on update thereof)
        Num cascadeWeightFactor;
        unsigned short cycleSize, cycleFracPercent;
        
        void submitCycle();
        bool shouldCycle();
        
        void account(typename Cyclic::iterator sample, bool additive);
    };
#undef _virt

#ifndef MOD_HISTORY_H_2PASS
#define MOD_HISTORY_H_2PASS
#include "History.h"
#else
#undef History
#endif

#endif /* HISTORY_H */

