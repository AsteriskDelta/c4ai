#ifndef MOD_ARITH_H
#define MOD_ARITH_H
#include "C4AI.h"

//namespace Modules {
    struct VectorArith_Root;
    struct ScalarArith_Root;
    
    template<typename T>
    T min(const T& a, const T& b, const std::enable_if<std::is_base_of<VectorArith_Root, T>::value, T> *const& unu = nullptr) {
        _unused(unu);
        T ret = a;
        for(const auto& smp : b) {
            if(!ret.has(smp.key)) ret[smp.key] = smp.value;
            else ret[smp.key] = std::min(ret[smp.key], smp.value);
        }
        
        return ret;
    }
    template<typename T>
    T max(const T& a, const T& b, const std::enable_if<std::is_base_of<VectorArith_Root, T>::value, T> *const& unu = nullptr) {
        _unused(unu);
        T ret = a;
        for(const auto& smp : b) {
            if(!ret.has(smp.key)) ret[smp.key] = smp.value;
            else ret[smp.key] = std::max(ret[smp.key], smp.value);
        }
        
        return ret;
    }
    template<typename T>
    T clamp(const T& val, const T& mi, const T& ma, 
            std::enable_if<std::is_base_of<VectorArith_Root, T>::value, T> *const& unu = nullptr) {
        _unused(unu);
        return max(mi, min(ma, val));
    }
    template<typename T>
    T sqrt(const T& a, const std::enable_if<std::is_base_of<VectorArith_Root, T>::value, T> *const& unu = nullptr) {
        _unused(unu);
        T ret;
        for(const auto& smp : a) {
            ret[smp.key] = ::sqrt(smp.value);
        }
        
        return ret;
    }
    template<typename T>
    T fabs(const T& a, const std::enable_if<std::is_base_of<VectorArith_Root, T>::value, T> *const& unu = nullptr) {
        _unused(unu);
        T ret;
        for(const auto& smp : a) {
            ret[smp.key] = ::fabs(smp.value);
        }
        
        return ret;
    }
    /*
    template<typename T>
    T min(const T& a, const T& b, const std::enable_if<!std::is_base_of<VectorArith_Root, T>::value, T> *const& unu = nullptr) {
        return std::min(a,b);
    }
    template<typename T>
    T max(const T& a, const T& b, const std::enable_if<!std::is_base_of<VectorArith_Root, T>::value, T> *const& unu = nullptr) {
        return std::max(a,b);
    }*/
//}

#endif
