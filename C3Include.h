#ifndef C3AI_INCLUDE
#define C3AI_INCLUDE

#include "C3Externals.h"
namespace C3AI {
    using namespace arke;
};

#ifndef _instantiator
#define _instantiator weak
#endif 

#ifndef _mutate
#define _mutate(FN, ARGS, VARS) \
inline auto FN ARGS {\
    const auto *const cthis = &const_cast<const decltype(*this)&>(*this);\
    using RetType = typename std::remove_const<typename std::remove_reference<decltype(cthis->FN VARS)>::type>::type;\
    return const_cast<RetType>(cthis->FN VARS);\
}
#define _rmutate(FN, ARGS, VARS) \
inline auto FN ARGS {\
    const auto *const cthis = &const_cast<const decltype(*this)&>(*this);\
    using RetType = typename std::remove_const<typename std::remove_reference<decltype(cthis->FN VARS)>::type>::type;\
    /*std::cout << decltype(const_cast<RetType&>(cthis->FN VARS))::blah << "\n";*/\
    /*std::cout << "RMT " << __PRETTY_FUNCTION__ << "\n";*/\
    return const_cast<RetType&>(cthis->FN VARS);\
}
#endif


#include <NVX/NVX.h>
#include <ARKE/Time.h>
namespace C3AI {
    using namespace nvx;
    typedef double Num;
    //typedef Num Time;
    using arke::Time;
    
    class Symbol;
    class Region;
    class Symlink;
    class Intrinsic;
    
    typedef Symbol* SymbolPtr;
    typedef Region* RegionPtr;
};

extern Spin::Group WorkGroup;

#ifndef _isnan
#define _isnan(val) (\
    ((val <= 0) == (val > 0))\
    )
#endif

#ifndef STR_MULTIREP
#define STR_MULTIREP
#include <string>
#include <algorithm>
namespace std {
    inline std::string& replace_all(std::string& str, const std::string& from, const std::string& to) {
        std::size_t start_pos = 0;
        while((start_pos = str.find(from, start_pos)) != std::string::npos) {
            str.replace(start_pos, from.length(), to);
            start_pos += to.length(); // ...
        }
        return str;
    }
};
#endif

#endif
